# Security Crisis in Mali

An analysis of the security crisis in Mali through numbers.


## How do I run the program ?

* Clone the project and navigate to the root directory
* Install requirements : 

```
> pip install -r requirements.txt
```
* Run the program with arguments. Example :
```
> python scrap_rfi_articles.py -f 2021-08-14 -t mali -k transition
```

See image **example.PNG** for an output example. The csv file is saved on the same directory. Some example are already available in the repo.

## Project's steps
- [x] Enabling extraction of any article's content **from** a given date **to** now according to *tag* and *keyword* parameters.
- [ ] Doing nlp with article's text to extarct relevant information (this is the next step).
