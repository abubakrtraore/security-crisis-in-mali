import numpy as np
import pandas as pd
import requests as rq

import sys, getopt
import re

from datetime import datetime
from newspaper import Article

from tqdm import tqdm

# Starting program
print('Starting program...')
start_time = datetime.now() 

# get args from launch

# for request
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

################################### All functions ##########################################
def get_links_and_dates(url, links_pattern='<a href="(.*)" data-article-item-link', dates_pattern='<time datetime="(.*)">', titles_pattern='(<p class="article__title ">(.*)</p>|</svg>\n</span>(.*)</p>)'):
    pager = rq.get(url, headers=headers).text
    # get all links in this page
    links_list = re.findall(links_pattern, pager)
    
    # get all links in this page
    titles_list = re.findall(titles_pattern, pager)
    
    #for dates
    dates_list = re.findall(dates_pattern, pager)
    
    return links_list, titles_list, dates_list


# get articles from RFI
def collect_articles(_from='2010-01-01', _to='now', tag='mali', extract_content=True, title_keyword=None, save_collected=True):
    print('Collecting first page informations...\n')
    start_url = 'https://www.rfi.fr/fr/tag/' + tag + '/'
    
    links_list, titles_list, dates_list = get_links_and_dates(start_url)
    
    if _to=='now' : #set the datetime now from system
        _to = datetime.now()
    
    page_number = 1
    try:
        print('Starting next pages collection : ', end='')
        while _from < dates_list[-1]:
            print(str(page_number+1) + '...', end='')
            current_url = 'https://www.rfi.fr/fr/tag/'+tag+'/'+str(page_number+1)+'/#pager'
            page_links, page_titles, page_dates = get_links_and_dates(current_url)


            #MAJ of links
            links_list  += page_links
            dates_list  += page_dates
            titles_list += page_titles

            page_number+= 1 #increment pager
            
    except Exception as e:
        print(e)
        
    print('End of Collection given date range : from %s to now'%_from)
    print('%d links collected'%(len(links_list)))
    
    print('Now, filtering links to get "only articles" according to the keyword : "%s"'%title_keyword)
    
    collected_data = pd.DataFrame({'article_link': links_list, 'title': [t[1] for t in titles_list], 'date': dates_list})
    collected_data['is_article']   = np.where(collected_data.title=='', False, True)
    collected_data['article_link'] = ["https://www.rfi.fr/" + l for l in collected_data.article_link] #adding rfi ref to link
    
    #now, we'll deal only with articles, not podcast or videos
    collected_articles_data = collected_data[collected_data.is_article].reset_index(drop=True).copy()
    
    if save_collected : 
        collected_data.to_csv('collected_data_with_tag_mali.csv', sep=';', index=False, encoding='utf-8')
        print('Collected Data has been successfully saved !')
    
    if title_keyword: 
        collected_articles_data = collected_articles_data[collected_articles_data.title.str.contains(title_keyword, case=False)].reset_index(drop=True)
    
    print('%d articles matching to keyword'%len(collected_articles_data))
    
    if extract_content:
        print('Extracting content of all %d articles'%len(collected_articles_data))
        contents, tags = [], []
        for link in tqdm(collected_articles_data.article_link):
            article = Article(link)
            article.download()
            article.parse()
            contents.append(article.text)
            tags    .append(article.tags)
        
        # add extracted content to data
        collected_articles_data['content'] = contents
        collected_articles_data['tags']    = tags
    
    return  collected_articles_data


if __name__=='__main__':
    
    argv = sys.argv[1:]

    try:
      opts, args = getopt.getopt(argv,"f:t:k:",["from=", "tag=", "keyword="])
    except getopt.GetoptError as err:
        print(err)
        print('Usage : python scrap_rfi_articles.py -f <from_date> -t <tag> -k <keyword>')
        sys.exit(2)
    
    for opt, arg in opts:
        if opt in ['-f', '--from']:
            _from = arg
        elif opt in ['-t', '--tag']:
            tag = arg
        elif opt in ['-k', '--keyword']:
            title_keyword = arg
        

    # call the main function
    all_data = collect_articles(_from=_from, tag=tag, extract_content=True, title_keyword=title_keyword)

    print('Saving file "final_data_with_keyword" to csv...')
    all_data.to_csv('final_data_with_keyword_'+title_keyword+'.csv', sep=';', index=False, encoding='utf-8-sig')
    print('File Saved.')

    time_elapsed = datetime.now() - start_time 

    print('Time elapsed (hh:mm:ss.ms) {}'.format(time_elapsed))

    print('Exiting...✔️')
